#!/bin/bash
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
B2_ACCOUNT_ID=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_id')
B2_ACCOUNT_KEY=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_key')
B2_BUCKET=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_bucket')
B2_PATH=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_path')
RESTIC_PASSWORD=$(cat $SCRIPTPATH/settings.json | jq -r '.encryption_password')
ACK_URL=$(cat $SCRIPTPATH/settings.json | jq -r '.hc_backup_url')

curl $ACK_URL/start
set -o pipefail
echo "==== UNLOCKING ====" | tee output
GOGC=20 RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic --verbose -r b2:$B2_BUCKET:$B2_PATH unlock
echo "==== BACKUP ====" | tee output
GOGC=20 RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic --verbose -r b2:$B2_BUCKET:$B2_PATH backup / --exclude-file $SCRIPTPATH/exclude-list.txt 2>&1 | tee output
backupStatus=$?
echo "==== PRUNING OLD BACKUPS ====" | tee output
GOGC=20 RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic --verbose -r b2:$B2_BUCKET:$B2_PATH forget --keep-last 40 --keep-weekly 16 --keep-monthly 8 --prune | tee output
output=$(cat output)
echo $output > $SCRIPTPATH/logs/backup-$(date +"%Y-%m-%d_%H-%M").log
trimmedOutput=$(echo $output | tail --bytes=10000)
curl $ACK_URL/$backupStatus --data-raw "$trimmedOutput"

#If SMTP is enabled we can send an email with the status of the backup
SMTP_ENABLE=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_enable')
if [ "$SMTP_ENABLE" = "true" ]; then
    MAIL_STATUS="ERROR"
    if [ "$backupStatus" -eq "0" ]; then
        MAIL_STATUS="SUCCESS"
    fi
    SMTP_HOST=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_host')
    SMTP_FROM=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_from')
    SMTP_TO=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_to')
    SMTP_USERNAME=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_username')
    SMTP_PASSWORD=$(cat $SCRIPTPATH/settings.json | jq -r '.smtp_password')
    curl    --url "smtps://$SMTP_HOST" --ssl-reqd \
            --mail-from "$SMTP_FROM" \
            --mail-rcpt "$SMTP_TO" \
            --user "$SMTP_USERNAME:$SMTP_PASSWORD" \
            -T <(echo -e "From: $SMTP_FROM\nTo: $SMTP_TO\nSubject: Backup $MAIL_STATUS for $(hostname)\n\n$output")

fi
