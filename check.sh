#!/bin/bash
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
B2_ACCOUNT_ID=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_id')
B2_ACCOUNT_KEY=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_key')
B2_BUCKET=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_bucket')
B2_PATH=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_path')
RESTIC_PASSWORD=$(cat $SCRIPTPATH/settings.json | jq -r '.encryption_password')
ACK_URL=$(cat $SCRIPTPATH/settings.json | jq -r '.hc_check_url')

curl $ACK_URL/start
#TODO: When restic landes 0.12 on Debian/Ubuntu use the random percentage
set -o pipefail
GOGC=20 RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic --verbose -r b2:$B2_BUCKET:$B2_PATH unlock
GOGC=20 RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic --verbose -r b2:$B2_BUCKET:$B2_PATH check 2>&1 | tee output
backupStatus=$?
output=$(cat output)
echo $output > $SCRIPTPATH/logs/check-$(date +"%Y-%m-%d_%H-%M").log
trimmedOutput=$(echo $output | tail --bytes=10000)
curl $ACK_URL/$backupStatus --data-raw "$trimmedOutput"
