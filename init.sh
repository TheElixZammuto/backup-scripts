#!/bin/bash
set -e
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
B2_ACCOUNT_ID=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_id')
B2_ACCOUNT_KEY=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_key')
B2_BUCKET=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_bucket')
B2_PATH=$(cat $SCRIPTPATH/settings.json | jq -r '.b2_path')
RESTIC_PASSWORD=$(cat $SCRIPTPATH/settings.json | jq -r '.encryption_password')
RESTIC_PASSWORD=$RESTIC_PASSWORD B2_ACCOUNT_ID=$B2_ACCOUNT_ID B2_ACCOUNT_KEY=$B2_ACCOUNT_KEY restic -r b2:$B2_BUCKET:$B2_PATH init